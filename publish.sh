#!/bin/bash

build_status () {
  local __result=$1
  local __output=$(exp build:status)
  while read -r line; do
    echo $line
  done <<< "$__output"
  eval $__result="'$line'"
}

while true; do
  build_status result
  if [ "$result" != "\[exp\] Android: Build in progress..." -o "$result" != "\[exp\] Android: Build waiting in queue..." ]; then
    echo "$result"
    break;
  fi;
  sleep 5;
done;
